# SSC0713_Sistemas-Evolutivos-Aplicados-a-Robotica
Disciplina SSC0713 - Sistemas Evolutivos e Aplicados à Robótica

Turma 2019:
Insira seu projeto no arquivo do GoogleDocs: https://docs.google.com/document/d/13UQ7g2X4aJEFjllzGfxCQWnqo3FhAhkfRTEuN_Dv7WA/edit?usp=sharing


### Lista de projetos 2019:

- Ecossistema com predador e presa: Ygor Pontelo, Marcelo Temoteo e Matheus Lucas - https://github.com/ygorpontelo/ecosystem
- Teste Feliz - Tiago Triques https://gitlab.com/tiagotriques/banana
- A Saga do Herói - Clara Rosa Silveira, Lucas Fernandes Turci, Raphael Medeiros Vieira https://github.com/lucasturci/ProjetoEvolutivos
- Tom and Jerry - Tiago Marino Silva, Alexandre Galocha Pinto Junior, Eduardo Pirro - https://github.com/firewall1011/tom-and-jerry
- A Lenda de Baiacu - Gyovana Mayara Moriyama, Henrique Matarazo Camillo
https://github.com/HenriqueCamillo/A-Lenda-de-Baiacu/
- PathPlanning with Obstacles - Pedro Natali, Rafael Pinho e Carlos Franco - https://github.com/PedroNatali/Trabalho-Evolutivos
- Flappy Bird - Gustavo Verniano Angélico de Almeida, Caio Ferreira Bernardo - https://github.com/CaioFerreiraB/trab_evolutivos
- Tetris Evolution - Rafael Gongora Bariccatti - https://github.com/bariccattion/Tetris_Evolution
- Problema das N rainhas
https://github.com/felfipe/trabsimoes
- Coevolução: predador e presa - Alice Valença De Lorenci
https://github.com/AliceDeLorenci/pray-predator-coevolution
- Mountain Car - Caio Ostan, Leandro Giusti Mugnaini, Leonardo Vinicius de Oliveira Toledo
https://github.com/LeoToledo/MountainCarGenetico
- Super Gravitron AG Edition - Augusto Ribeiro Castro, Estevam Fernandes Arantes
https://github.com/Es7evam/Super-Gravitron-AG-Edition
- Houdini metaballs - Juliana Crivelli
https://github.com/jumc/houdini-genetic-optimization
- Problema das 8 Rainhas - Guilherme Brunassi Nogima, Leonardo Akel Daher
https://github.com/gbnogima/8queens
- Smart Rockets - Daniel Sivaldi Feres, Gabriel Scalici
https://github.com/GabrielScalici/Foguetes_Sistemas_Evolutivos
- Melhor caminho com obstáculos - Guilherme Blatt, Igor Rodrigues
https://github.com/guilherme-blatt/Caminho-AG


### Lista de projetos 2018:
- Genetic Max Value Calculator: André Fakhoury, Thiago Preischadt, Vitor Santana - https://andrefakhoury.github.io/genetic-max-value-calculator/
- Genetic clustering framework: Daniel Barretto, David Cairuz, João Guilherme Araújo, Luísa Moura - https://github.com/lusmoura/Darwin
- Neuroevolutional Car Racing game: Lucas Mitri, Lucas Marcondes, Luis Ricardo - https://github.com/lucasgdm/neuroevolution-car-racing 🏎️
- NEAT SuperHexagon + gambiarras do Windows: Leonardo Gomes, André Almada, Eduardo Misiuk, Ali Husseinat - https://github.com/leoagomes/super-intelligence
- Simon's Evolution (Tron tira a tampa!): Eleazar Braga, Fabrício Guedes, Gustavo Lopes - https://github.com/fabricio-gf/tira-a-tampa
- Circuit Optimizer: Guilherme Prearo, Gustavo Nicolau, Paulo Augusto, Vianna - https://github.com/gprearo/CircuitOptimizer
- Slither IO - Snake Evolution: Paulo Augusto, Guilherme Prearo, Gustavo Nicolau -https://github.com/Kotzly/Slither_IO_IA
- Elections in Brazil (Apresentacao em Video https://www.youtube.com/watch?v=eg5iIUG49T0) : Marilene Garcia, Maria Luisa do Nascimento - https://github.com/MariaLuisaNascimento/Trabalho-AlgEvo
- Image Aproximator: Bruno Bacelar Abe, Paula Cepollaro Diana - https://github.com/abe2602/EvolvingAlgs
- FlappyBird e Vinho: Andre Daher Benedetti, Marcelo Bertoldi Diani - https://github.com/MarceloBD/sistemasEvolutivos 
- Snake AI: Paulo Pinheiro Lemgruber Jeunon Sousa, Matheus Carvalho Nali - https://github.com/PauloLemgruberJeunon/AI_Snake
- Caixeiro Viajante (TSP): Murilo Baldi, Victor Roberti Camolesi - https://github.com/Murgalha/tsp-ga
- We Are The Robots (ainda incompleto): Atenágoras Souza Silva - https://gitlab.com/atenagoras/We_Are_The_Robots
- Simplex Evolutivo: Bruno Flávo, Edson Yudi, Rafael Amaro Rolfsen - https://github.com/Exilio016/AG-Solver
- Multithreaded Genetic Algorithm: Gabriel Romualdo Silveira Pupo - https://github.com/gabrielrspupo/multithread-ga
- Tic Tac Toe AI: Luís Eduardo Rozante de Freitas Pereira - https://github.com/LuisEduardoR/tic-tac-toe-genetic-algorithm
- Genetic Minimax Checkers/Racing Car Evolution: Gabriel Carvalho, Victor Souza Cezario - https://github.com/victorcezario97/GACheckers.git (Checkers) / https://github.com/GabrielBCarvalho/Evolutionary-Algorithm-Car (Racing Car)
- A.G. com redeus neurais (jogo da velha): Bráulio Bezerra, Samuel Ferreira, João Ramos, Victor Giovannoni - https://github.com/brauliousp/jogo_da_velha
- Vida de Inseto: Danilo Henrique Cordeiro, Gabriel Kanegae Souza, Marcos Vinicius Barros de Lima Andrade Junqueira - https://github.com/Dancorde/evolutionary-steering (link do trabalho rodando: https://dancorde.github.io/evolutionary-steering/)
- Sliding Puzzle AG: Gabriel Toschi de Oliveira, Ana Maia Baptistão, Fernanda Tostes Marana - https://github.com/gabtoschi/slidingpuzzleGA
- Ant Collony: David Souza Rodrigues, Marcos Wendell Souza de Oliveira Santos - https://github.com/MarcosWendell/Algoritmos_Evolutivos
- Tibia Neat: Alex Sander R. Silva - https://github.com/Psycho-Ray/Tibia-Neat
- FlappyBird: Bolinha Preta: Giovanni Attina do Nascimento, Paulo Andre Carneiro - https://github.com/PauloCarneiro99/Flappy-Bird
- Cinturão de Asteroides: Lucas Nobuyuki Takahashi, Marcelo Kiochi Hatanaka, Paulo Renato Campos Barbosa - https://github.com/pauloty/Cinturao-De-Asteroides
- FoguetesEvolutivos: Alexandre Norcia Medeiros, Giovani Decico Lucafo, Luiz Henrique Lourencao - https://github.com/alexandrenmedeiros/FoguetesEvolutivos
- Neurogen Snake: Guilherme Milan Santos - https://github.com/guilhermesantos/neurogen_snake
- lo e 248 AI, Miguel Gardini, João Pedro Mattos, Vitor Rossi Speranza - https://github.com/vrsperanza/2048-AI---Evolutive-algorithm
- Ploty , Vitor Santana Cordeiro, Thiago Preischadt Pinheiro, André Luís Mendes Fakhoury, - https://github.com/andrefak/genetic-algorithm-plotly
- ProblemaMaximizarItensCaminhao, Helen Santos Picoli - https://github.com/helenspicoli/AlgGenProject
- Space Invaders: Guilherme de Pinho Montemovo, Igor Barbosa Grécia Lúcio, Rafael Corradini da Cunha - https://github.com/rafaelcorradini/space-invaders-genetic-ia
- Black Jack, Guilherme dos Reis, Tiago Daneluzzi - https://github.com/daneluzzitiago/21_evolutivo
- Ecossistema com predador e presa: Ygor Pontelo, Marcelo Temoteo e Matheus Lucas - https://github.com/ygorpontelo/ecosystem
- Próximo Grupo ...

